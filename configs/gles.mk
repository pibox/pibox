# ---------------------------------------------------------------
# OpenGLES: the "userland" tools that provide passthrough to the binary
# blob firmware files.
# ---------------------------------------------------------------
#  Cleaning out the gles stuff requires root access cuz its a stupid build system.  
$(GLES_T)-verify: 
	@if [ $(UID) -ne 0 ]; then \
		echo "You must be root to run this target."; \
		exit 1; \
	fi

# Retrieve package
$(GLES_T)-get .$(GLES_T)-get:
	@mkdir -p $(BLDDIR) $(GLES_ARCDIR)
	@if [ ! -d $(GLES_ARCDIR)/$(GLES_CLONEDIR) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) "Retrieving GLES (userland) source" $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(GLES_ARCDIR) && git clone $(GLES_URL) $(GLES_ARCDIR)/$(GLES_CLONEDIR); \
	else \
		$(MSG3) $(GLES) source is cached - updating to latest $(EMSG); \
		cd $(GLES_ARCDIR)/$(GLES_CLONEDIR) && git checkout master; \
		cd $(GLES_ARCDIR)/$(GLES_CLONEDIR) && git pull; \
	fi
	cd $(GLES_ARCDIR)/$(GLES_CLONEDIR) && git checkout $(GLES_BRANCH)
	@touch .$(subst .,,$@)

# Unpack the package.  Since it's a git clone, we just copy it.
# Also grab, the PiBox versions of configuration files.
.$(GLES_T)-unpack $(GLES_T)-unpack : .$(GLES_T)-get
	@if [ ! -d $(GLES_SRCDIR) ]; then \
		if [ -d $(GLES_ARCDIR)/$(GLES_CLONEDIR) ]; then \
			$(MSG3) "Unpacking $(GLES) source" $(EMSG); \
			cp -r $(GLES_ARCDIR)/$(GLES_CLONEDIR) $(BLDDIR); \
		else \
			$(MSG11) "$(GLES) source archive is missing:" $(EMSG); \
			$(MSG11) "$(GLES_ARCDIR)/$(GLES_CLONEDIR) not found " $(EMSG); \
			exit 1; \
		fi; \
	fi
	@cp $(DIR_GLES)/buildme.pibox $(GLES_SRCDIR)/
	@sed -i 's%\[XI\]%$(XCC_PREFIXDIR)%g' $(GLES_SRCDIR)/buildme.pibox
	@cp $(DIR_GLES)/pibox.cmake $(GLES_SRCDIR)/makefiles/cmake/toolchains/
	@sed -i 's%\[XCCPREFIX\]%$(XCC_PREFIX)%g' $(GLES_SRCDIR)/makefiles/cmake/toolchains/pibox.cmake
	@sed -i 's%\[MARCH\]%$(MARCH)%g' $(GLES_SRCDIR)/makefiles/cmake/toolchains/pibox.cmake
	@sed -i 's%\[MCPU\]%$(MCPU)%g' $(GLES_SRCDIR)/makefiles/cmake/toolchains/pibox.cmake
	@sed -i 's%\[MFPU\]%$(MFPU)%g' $(GLES_SRCDIR)/makefiles/cmake/toolchains/pibox.cmake
	@cp $(DIR_GLES)/CMakeLists.txt $(GLES_SRCDIR)/host_applications/linux/libs/bcm_host
	@touch .$(subst .,,$@)

$(GLES_T)-patch .$(GLES_T)-patch: .$(GLES_T)-unpack
	@if [ -d $(DIR_GLES)/patches/ ]; then \
		for patchname in `ls -1 $(DIR_GLES)/patches/*.patch`; do \
			$(MSG3) Applying $$patchname $(EMSG); \
			cd $(GLES_SRCDIR) && patch -Np1 -r - < $$patchname; \
		done; \
	fi
	@touch .$(subst .,,$@)

$(GLES_T)-init .$(GLES_T)-init: .$(GLES_T)-patch 
	@mkdir -p $(GLES_SRCDIR)/target/opt/vc/include 
	@if [ ! -h $(GLES_SRCDIR)/target/opt/vc/include/vchost_config.h ]; then \
		cd $(GLES_SRCDIR)/target/opt/vc/include && ln -s interface/vmcs_host/linux/vchost_config.h .; \
	fi
	@touch .$(subst .,,$@)

.$(GLES_T) $(GLES_T): .$(GLES_T)-init
	@$(MSG) "================================================================"
	@$(MSG2) "Building $(GLES)" $(EMSG)
	@$(MSG) "================================================================"
	@mkdir -p $(GLES_SRCDIR)/target
	@cd $(GLES_SRCDIR) && /bin/bash buildme.pibox $(GLES_SRCDIR)/target
	@touch .$(subst .,,$@)

$(GLES_T)-pkg: .$(GLES_T)
	@$(MSG3) "No packages for $(GLES_T)" $(EMSG)

$(GLES_T)-clean:
	@rm -rf $(GLES_SRCDIR)
	@rm -f .$(GLES_T) .$(GLES_T)-init .$(GLES_T)-unpack .$(GLES_T)-patch

$(GLES_T)-clobber: $(GLES_T)-clean
	@rm -rf $(GLES_ARCDIR)
	@rm -f .$(GLES_T)-get

