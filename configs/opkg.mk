# ---------------------------------------------------------------
# Host OPKG utilities
# ---------------------------------------------------------------
# Retrieve package
$(OPKG_T)-get: 
	@mkdir -p $(BLDDIR) $(OPKG_ARCDIR)
	@if [ ! -d $(OPKG_ARCDIR)/$(OPKG_CLONEDIR) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) Retrieving opkg source $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(OPKG_ARCDIR) && git clone $(OPKG_URL) $(OPKG_ARCDIR)/$(OPKG_CLONEDIR); \
		cd $(OPKG_ARCDIR)/$(OPKG_CLONEDIR) && git checkout $(OPKG_BRANCH); \
	else \
		$(MSG3) opkg source is cached $(EMSG); \
		cd $(OPKG_ARCDIR)/$(OPKG_CLONEDIR) && git checkout $(OPKG_BRANCH); \
	fi

# Unpack the package.  Since it's a git clone, we just copy it.
$(OPKG_T)-unpack: $(OPKG_T)-get
	@if [ ! -d $(OPKG_SRCDIR) ]; then \
		if [ -d $(OPKG_ARCDIR)/$(OPKG_CLONEDIR) ]; then \
			$(MSG3) "Unpacking opkg source" $(EMSG); \
			cp -r $(OPKG_ARCDIR)/$(OPKG_CLONEDIR) $(BLDDIR); \
		else \
			$(MSG11) "opkg source archive is missing:" $(EMSG); \
			$(MSG11) "$(OPKG_ARCDIR)/$(OPKG_CLONEDIR) not found " $(EMSG); \
			exit 1; \
		fi; \
	fi

$(OPKG_T)-init: $(OPKG_T)-unpack 

$(OPKG_T): $(OPKG_T)-init
	@$(MSG) "================================================================"
	@$(MSG2) "Building opkg-utils" $(EMSG)
	@$(MSG) "================================================================"
	@cd $(OPKG_SRCDIR) && make 

# Install the tools 
$(OPKG_T)-install: 
	@$(MSG) "================================================================"
	@$(MSG2) "Building opkg-utils" $(EMSG)
	@$(MSG) "================================================================"
	@if [ $(UID) -ne 0 ]; then \
		echo "You must be root to install the opkg-utils."; \
		exit 1; \
	fi
	@cd $(OPKG_SRCDIR) && make install

# Check if the package is installed
$(OPKG_T)-verify: 
	@if [ ! -f /usr/local/bin/opkg-build ]; then \
		echo "opkg-utils does not appear to be installed"; \
		echo "Try \"make opkg-install\" "; \
		exit 1; \
	fi

$(OPKG_T)-pkg: 
	@$(MSG3) "No packages for $(OPKG_T)" $(EMSG)

$(OPKG_T)-clobber: 
	@rm -rf $(OPKG_SRCDIR)

$(OPKG_T)-clean:
	@if [ -d $(OPKG_SRCDIR) ]; then \
		cd $(OPKG_SRCDIR) && make clean; \
	fi

