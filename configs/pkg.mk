# ---------------------------------------------------------------
# Package everything.
# ---------------------------------------------------------------
$(PKG_T)-init: 
	@mkdir -p $(PKGDIR)

$(PKG_T): $(PKG_T)-src $(PKG_T)-scripts 
	@rm -f $(PKGDIR)/pibox-*.tar.gz
	@$(MSG) "================================================================"
	@$(MSG2) "Gathering package files" $(EMSG)
	@$(MSG) "================================================================"
	@for component in $(PKG_TARGETS); do \
		if [ "$$component" != "$(PKG_T)" ]; then \
			make --no-print-directory $$component-pkg; \
		fi; \
	done
	@$(MSG) "================================================================"
	@$(MSG2) "Package List" $(EMSG)
	@$(MSG) "================================================================"
	mv $(PKGDIR)/staging.tar.gz $(TOPDIR)
	mv $(PKGDIR) $(TOPDIR)/pibox-$(HW)-$(BLD_VERSION)
	cd $(TOPDIR) && tar -cf pibox-$(HW)-$(BLD_VERSION).tar pibox-$(HW)-$(BLD_VERSION)
	mv $(TOPDIR)/pibox-$(HW)-$(BLD_VERSION) $(PKGDIR)
	cd $(TOPDIR) && 7z a -m0=lzma2 -mx=9 $(PKGDIR)/pibox-$(HW)-$(BLD_VERSION).tar.7z pibox-$(HW)-$(BLD_VERSION).tar
	cd $(TOPDIR) && rm -f pibox-$(HW)-$(BLD_VERSION).tar
	mv $(TOPDIR)/staging.tar.gz $(PKGDIR)
	@ls -l $(PKGDIR)

$(PKG_T)-src: $(PKG_T)-init
	rm -f $(PKGDIR)/pibox-*-src.tar.gz
	mkdir $(PKGDIR)/pibox-$(PKG_VERSION)
	cd $(PKGDIR) && git clone git@gitlab.com:pibox/pibox.git pibox-$(PKG_VERSION)
	cd $(PKGDIR) && tar --exclude=.git -cvzf pibox-$(PKG_VERSION)-src.tar.gz pibox-$(PKG_VERSION)
	rm -rf $(PKGDIR)/pibox-$(PKG_VERSION)

# ---------------------------------------------------------------
# Component specific packaging 
# ---------------------------------------------------------------
scripts-$(PKG_T): $(PKG_T)-scripts

$(PKG_T)-scripts: $(PKG_T)-init
	@$(MSG) "================================================================"
	@$(MSG2) "Grabbing installation scripts" $(EMSG)
	@$(MSG) "================================================================"
	@cp $(SCRIPTDIR)/mk*.sh $(PKGDIR)
	@cp $(SCRIPTDIR)/qemu.sh $(PKGDIR)
	@cp $(DOCDIR)/install.txt $(PKGDIR)
ifeq ($(HW),rpi)
	@sed -i 's%^DESTUIMAGE=.*%DESTUIMAGE=\"kernel.img\"%' $(PKGDIR)/mkinstall.sh
endif
ifeq ($(HW),rpi2)
	@sed -i 's%^DESTUIMAGE=.*%DESTUIMAGE=\"kernel7.img\"%' $(PKGDIR)/mkinstall.sh
endif

$(PKG_T)-clean:
	@rm -rf $(PKGDIR) 

$(PKG_T)-clobber: $(PKG_T)-clean

