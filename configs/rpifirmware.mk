# ---------------------------------------------------------------
# Grab and install Raspberry Pi firmware images
# ---------------------------------------------------------------

.$(FIRMWARE_T)-get:
	@mkdir -p $(BLDDIR) $(FIRMWARE_ARCDIR) 
	@if [ ! -d $(FIRMWARE_ARCDIR)/$(FIRMWARE_VERSION) ]; then \
		$(MSG) "================================================================"; \
		$(MSG3) Retrieving firmware source $(EMSG); \
		$(MSG) "================================================================"; \
		cd $(FIRMWARE_ARCDIR) && git clone $(FIRMWARE_URL) $(FIRMWARE_VERSION); \
	else \
		$(MSG3) Firmware source is cached - pulling latest $(EMSG); \
		cd $(FIRMWARE_ARCDIR)/$(FIRMWARE_VERSION) && git pull $(FIRMWARE_URL) $(FIRMWARE_VERSION); \
	fi
	@$(MSG3) "Checking out $(FIRMWARE_GIT_ID)" $(EMSG)
	@cd $(FIRMWARE_ARCDIR)/$(FIRMWARE_VERSION) && git checkout $(FIRMWARE_GIT_ID)
	@touch .$(subst .,,$@)

# Get, unpack and patch kernel, as needed
.$(FIRMWARE_T)-init $(FIRMWARE_T)-init: .$(FIRMWARE_T)-get
	@touch .$(subst .,,$@)

.$(FIRMWARE_T): .$(FIRMWARE_T)-init 

$(FIRMWARE_T): .$(FIRMWARE_T)

$(FIRMWARE_T)-pkg: $(FIRMWARE_T)
	@$(MSG) "================================================================"
	@$(MSG2)" Grabbing firmware files and scripts " $(EMSG)
	@$(MSG) "================================================================"
	@mkdir -p $(PKGDIR)/firmware
	@cd $(FIRMWARE_ARCDIR)/$(FIRMWARE_VERSION)/boot && \
		cp -r * $(PKGDIR)/firmware
	@cd $(DIR_FIRMWARE) && \
		cp -r $(FIRMWARE_SCRIPT_LIST) $(PKGDIR)/firmware

$(FIRMWARE_T)-clean: 
	@rm -f .$(FIRMWARE_T)-init .$(FIRMWARE_T)-get .$(FIRMWARE_T)

$(FIRMWARE_T)-clobber:
	@make --no-print-directory -i $(FIRMWARE_T)-clean

