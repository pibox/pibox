#!/bin/bash
# Build a Docker environment suitable for building the
# PiBox Development Platform.
#--------------------------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
# Prefix with "P" to avoid namespace collisions.
PUID="$(id -u)"
PGID="$(id -g)"
PUNAME="$(id -u -n)"
PW=""
VERS=""
IMG_NAME="pibox_build"

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------

# Provide command line usage assistance
doHelp()
{
    echo ""
    echo "$0 [-u uid | -g gid | -n username] -p pw"
    echo "where"
    echo "-p pw       Required: Password to use when setting up user in Docker image."
    echo "-u uid      Numeric user id; Defaults to your uid: $(id -u)"
    echo "-g uid      Numeric group id; Defaults to your gid: $(id -g)"
    echo "-n username User name; Defaults to your username: $(id -u -n)"
    echo "-v version  Set a version string (defaults to no version string)"
    echo ""
    echo "See the README.txt for detailed build and use instructions for the PiBox docker image."
}

# Validate a string is numeric
notANum()
{
    value=$1
    re='^[0-9]+$'
    if ! [[ ${value} =~ ${re} ]] ; then
        return 0
    fi
    return 1
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":g:n:p:u:v:" Option
do
    case $Option in
    g) PGID="${OPTARG}";;
    n) PUNAME="${OPTARG}";;
    p) PW="${OPTARG}";;
    u) PUID="${OPTARG}";;
    v) VERS="${OPTARG}";;
    *) doHelp; exit 0;;
    esac
done

# Validate password
if [[ -z ${PW} ]]; then
    echo "Password option is required."
    doHelp
    exit 1
fi

# Validate uid
if notANum "${PUID}"; then
    echo "UID must be numeric."
    doHelp
    exit 1
fi

# Validate gid
if notANum "${PGID}"; then
    echo "GID must be numeric."
    doHelp
    exit 1
fi

# Validate username
if [[ -z ${PUNAME} ]]; then
    echo "Username must not be empty string."
    doHelp
    exit 1
fi

# Set a version number on the image name, if requested.
# This helps with testing new versions of the docker image.
if [[ -n "${VERS}" ]]; then
    IMG_NAME="${IMG_NAME}-${VERS}"
fi

#--------------------------------------------------------------
# Main
#--------------------------------------------------------------
sudo docker build \
    --build-arg UID="${PUID}" \
    --build-arg GID="${PGID}" \
    --build-arg UNAME="${PUNAME}" \
    --build-arg PW="${PW}" \
    -t ${IMG_NAME} ./
