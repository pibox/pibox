# Build RPi tools from PiBox
LDFLAGS=-L$(SDKSTAGE)/usr/lib
INSTALLDIR=$(SDKSTAGE)/opt/vc/bin

all:
	make -C libs/ilclient
	make -C libs/vgfont
	make -C libs/revision
	make -C hello_world
	make -C hello_triangle
	make -C hello_triangle2
	make -C hello_video
	make -C hello_audio
	make -C hello_dispmanx
	make -C hello_tiger
	make -C hello_encode
	make -C hello_jpeg

# Doesn't cross compile correctly
dummy:
	make -C hello_font clean
	make -C hello_font

clean:
	make -C libs/ilclient clean
	make -C libs/vgfont clean
	make -C libs/revision clean
	make -C hello_world clean
	make -C hello_triangle clean
	make -C hello_triangle2 clean
	make -C hello_video clean
	make -C hello_audio clean
	make -C hello_dispmanx clean
	make -C hello_tiger clean
	make -C hello_encode clean
	make -C hello_jpeg clean

install:
	cp ./hello_video/hello_video.bin $(INSTALLDIR)
	cp ./hello_triangle2/hello_triangle2.bin $(INSTALLDIR)
	cp ./hello_dispmanx/hello_dispmanx.bin $(INSTALLDIR)
	cp ./hello_encode/hello_encode.bin $(INSTALLDIR)
	cp ./hello_world/hello_world.bin $(INSTALLDIR)
	cp ./hello_triangle/hello_triangle.bin $(INSTALLDIR)
	cp ./hello_audio/hello_audio.bin $(INSTALLDIR)
	cp ./hello_jpeg/hello_jpeg.bin $(INSTALLDIR)
	cp ./hello_tiger/hello_tiger.bin $(INSTALLDIR)
