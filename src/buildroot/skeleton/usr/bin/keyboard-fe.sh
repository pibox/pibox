#!/bin/sh
# Load an xmodmap configuration for supported keyboards.
# This gets called by X initialization or by mdev.
# -----------------------------------------------------

# Do nothing if we don't have a supported keyboard list.
[ -e /etc/X11/kbd/kbd.list ] || exit 0

# This can be called outside the context of the X server initialization
# so this makes sure the X server knows we're talking to it.
export DISPLAY=unix:0.0

# Load a keyboard file.  This might load more than one if there
# are multiple keyboards installed - be careful!
while read line; do
    usbid="$(echo ${line} | cut -f1 -d" ")"
    kbdfile="$(echo ${line} | cut -f2 -d" ")"

    # Check if we've already loaded this at least once.
    active="$(grep "${usbid}" /etc/X11/kbd/active)"
    if [ -n "${active}" ]
    then
        continue
    fi
    
    # Check if this USB ID is available to the system.
    kbd="$(lsusb | grep "${usbid}")"
    if [ -n "${kbd}" ]
    then
        # Make sure the configuration file is available.
        if [ -e "/etc/X11/kbd/${kbdfile}" ]
        then
            # Load the config and make a note that it's been loaded.
            sh "/etc/X11/kbd/${kbdfile}"
            echo "Keyboard: ${usbid} /etc/X11/kbd/${kbdfile}" >> "/etc/X11/kbd/active"
        fi
    fi
done < /etc/X11/kbd/kbd.list
