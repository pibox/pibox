# Raspberry Pi 7" Touchscreen
dtoverlay=rpi-ft5406
dtoverlay=rpi-backlight
lcd_rotate=2
dmi_ignore_edid=0xa5000080
config_hdmi_boost=7
gpu_mem=320
hdmi_group=2
hdmi_mode=87
hdmi_cvt=800 480 60 6 0 0 0
hdmi_drive=1
