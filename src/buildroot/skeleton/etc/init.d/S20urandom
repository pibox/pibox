#! /bin/sh
#
# Initialize RNG 
#

SEED_FILE="/etc/random-seed"
POOLFILE="/proc/sys/kernel/random/poolsize"

[ -c /dev/urandom ] || exit 0

case "$1" in

    start|"")
        echo "Initializing random number generator..."

        # Seed the RNG
        if [ -f "${SEED_FILE}" ]; then
            cat "${SEED_FILE}" >/dev/urandom
        else
            touch "${SEED_FILE}"
        fi

        # Note the new available entropy otherwise we'll use it up in the next step.
        # "command -v" is portable "which"
        if command -v pbsetentropy >/dev/null 2>&1; then
            pbsetentropy -d "$(cat "${POOLFILE}")"
        fi

        chmod 600 "${SEED_FILE}"
        [ -r "${POOLFILE}" ] && bits=$(cat "${POOLFILE}") || bits=4096
        bytes=$(( bits / 8 ))
        dd if=/dev/urandom of="${SEED_FILE}" count=1 bs=${bytes}
        ;;

    stop)
        [ -r "${POOLFILE}" ] && bits=$(cat "${POOLFILE}") || bits=4096
        bytes=$(( bits / 8 ))
        dd if=/dev/urandom of="${SEED_FILE}" count=1 bs=${bytes}
        ;;

    *)
        echo "Usage: urandom {start|stop}" >&2
        exit 1
        ;;
esac
