#!/bin/sh
#
# Starts lighttpd.
#
NAME=lighttpd
DAEMON=/usr/sbin/$NAME
PID_FILE="/var/run/$NAME.pid"
CONF_FILE="/etc/$NAME/$NAME.conf"
LOG_DIR="/var/log/lighttpd"
SOCK_DIR="/var/run/lighttpd"

[ -r /etc/default/$NAME ] && . /etc/default/$NAME

start() {
    # Create lighttpd's log directory if not already there.
    if [ ! -d "${LOG_DIR}" ]
    then
        mkdir -p "${LOG_DIR}"
        # lighttpd runs as user nobody.nobody
        chown nobody.nobody "${LOG_DIR}"
    fi
    if [ ! -d "${SOCK_DIR}" ]
    then
        mkdir -p "${SOCK_DIR}"
        # lighttpd runs as user nobody.nobody
        chown nobody.nobody "${SOCK_DIR}"
    fi
    printf "Starting lighttpd: "
    start-stop-daemon -S -q -p $PID_FILE --exec $DAEMON -- -f $CONF_FILE
    echo "OK"
}
stop() {
    printf "Stopping lighttpd: "
    start-stop-daemon -K -q -p $PID_FILE
    echo "OK"
}
restart() {
    stop
    start
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  restart|reload)
    restart
    ;;
  *)
    echo "Usage: $0 {start|stop|restart}"
    exit 1
esac

exit $?

