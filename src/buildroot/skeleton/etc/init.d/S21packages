#!/bin/sh

# shellcheck disable=SC1091
. /etc/init.d/functions

# These will override settings in functions script.
PKGDIR=/media/mmcblk0p3/opkg/pkg
INCOMINGDIR=/media/mmcblk0p3/opkg/incoming
PKGSTAMP=${INCOMINGDIR}/.install
REBOOTSTAMP=${INCOMINGDIR}/.reboot
LOGFILE=install.log

start() {
    # Check if package installation should be run.
    # Run quietly.
    if [ ! -e ${PKGSTAMP} ]; then
        logger "No packages to install."
        return;
    fi
    mkdir -p "${LOGDIR}/opkg"

    # Install any packages in the incoming package directory.
	printf "Starting package services: "
    cp /var/log/messages "${LOGDIR}/opkg/messages.1"
    opkg install ${INCOMINGDIR}/*.opk 2>&1 | logger
    cp /var/log/messages "${LOGDIR}/opkg/messages.2"
    # shellcheck disable=SC2181
    if [ $? -eq 0 ]; then
        mv ${INCOMINGDIR}/* ${PKGDIR}
        rm ${PKGSTAMP}
        touch ${REBOOTSTAMP}
    fi

    # Save package install logs only. Subshell keeps shellcheck happy.
    (
        # shellcheck disable=SC2164
        cd "${LOGDIR}/opkg"
        DATE="$(date +%Y%m%d.%H%M%S)"
        grep -vxF -f messages.1 messages.2 > "${LOGFILE}-${DATE}"
        rm messages.1 messages.2
        cp /etc/opkg/status "status-${DATE}"
    )
}

case "$1" in
  start)
  	start
	;;
  stop)
	;;
  *)
	echo "Usage: $0 {start|stop}"
	exit 1
esac

exit $?
