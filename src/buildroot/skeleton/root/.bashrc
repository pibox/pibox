export TZ=`cat /etc/TZ`
alias ll='ls -alF'
PAGER=${PAGER:-less}
export PAGER
export EDITOR=vi
set -o vi			# uses vi cmd line edits

ttyport="tty`tty | cut -d"/" -f4`"
PS1='[36m$USER([33m${ttyport}[36m)[0m$ '
