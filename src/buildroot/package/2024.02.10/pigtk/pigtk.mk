#############################################################
#
# pigtk
#
#############################################################
PIGTK_VERSION = main
PIGTK_SOURCE = pigtk-$(PIGTK_VERSION).tar.gz
PIGTK_SITE = https://gitlab.com/pibox/pigtk/-/archive/$(PIGTK_VERSION)
PIGTK_DEPENDENCIES = host-pkgconf libgtk2
PIGTK_AUTORECONF = YES
PIGTK_TOOL_PATCH = YES
PIGTK_INSTALL_STAGING = YES
PIGTK_LICENSE_FILES = COPYING

define PIGTK_PRE_CONFIGURE_FIXUP
    cd $(@D) && autoreconf -i
endef

PIGTK_PRE_CONFIGURE_HOOKS += PIGTK_PRE_CONFIGURE_FIXUP

$(eval $(autotools-package))
