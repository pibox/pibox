#############################################################
#
# pibox-network-config
#
#############################################################
PIBOX_NETWORK_CONFIG_VERSION = master
PIBOX_NETWORK_CONFIG_SITE = https://gitlab.com/pibox/pibox-network-config/-/archive/$(PIBOX_NETWORK_CONFIG_VERSION)
PIBOX_NETWORK_CONFIG_SOURCE = pibox-network-config-$(LIBPIBOX_VERSION).tar.gz
PIBOX_NETWORK_CONFIG_DEPENDENCIES = host-pkgconf libgtk2 libpibox
PIBOX_NETWORK_CONFIG_AUTORECONF = YES
PIBOX_NETWORK_CONFIG_LIBTOOL_PATCH = YES
PIBOX_NETWORK_CONFIG_INSTALL_STAGING = YES
PIBOX_NETWORK_CONFIG_LICENSE_FILES = COPYING

define PIBOX_NETWORK_CONFIG_PRE_CONFIGURE_FIXUP
    cd $(@D) && autoreconf -i
endef

PIBOX_NETWORK_CONFIG_PRE_CONFIGURE_HOOKS += PIBOX_NETWORK_CONFIG_PRE_CONFIGURE_FIXUP

$(eval $(autotools-package))
