FBTFT     = fbtft
FBTFT_URL = https://github.com/notro/fbtft.git
FBTFT_DIR = drivers/video/

$(FBTFT)-get:
	if [ ! -d $(KERNEL_SRCDIR)/$(FBTFT_DIR)/$(FBTFT) ]; \
	then \
		mkdir -p $(KERNEL_SRCDIR)/$(FBTFT_DIR); \
		cd $(KERNEL_SRCDIR)/$(FBTFT_DIR) && git clone $(FBTFT_URL) $(FBTFT); \
	fi
