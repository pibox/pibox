#!/bin/bash -p
# 
# Copy the appropriate files, in the correct order,
# to the boot partition on the SD card.
# Then, if requested, copy the rootfs to its partition.
#
# Order of install:
# Raspberry Pi firmware
# kernel.img - Linux Kernel
# special cmdline.txt - Linux kernel configuration options
# special config.txt - Raspberry Pi bootloader configuration options
# rootfs - Root file system as tar.gz archive
# ------------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
BOOTDIR=none
ROOTFSDIR=none
SDROOTFS=/media/bbrootfs
FIRMWARE=firmware
IMGFILE=
DOFILE=0
DOOVERLAY=0
DOBOOTSCR=0
DOUENV=0
HW="any"
DEVICE=""

# These file names are configurable
UIMAGE="kernel.img"
DESTUIMAGE="kernel.img"
ROOTFSARCHIVE="rootfs.squashfs"
BOOTSCR="${FIRMWARE}/cmdline.txt"
UENV="${FIRMWARE}/config.txt"

#--------------------------------------------------------------
# Provide command line usage assistance
#--------------------------------------------------------------
doHelp()
{
    echo ""
    echo "$0 [-o | -b <dir> | -d <device> | -l <file> | -s <file> | -t <file> | -r <file> | -f <file> | -h <type> ]"
    echo "where"
    echo "-b <dir>     Directory where boot partition is mounted; Default: ${BOOTDIR}"
    echo "-d <device>  Device (such as /dev/sdb2) for root file system; Default: ${ROOTFSDIR}"
    echo "             Set this to \"none\" to skip installing the rootfs"
    echo "-l <file>    Image file to use for Linux kernel; Default: ${UIMAGE}"
    echo "-s <file>    Custom config.txt; Default: ${BOOTSCR}"
    echo "-t <file>    Custom cmdline.txt; Default: ${UENV}"
    echo "-r <file>    Archive file for root file system; Default: ${ROOTFSARCHIVE}"
    echo "-f <file>    Image file to use instead of a device"
    echo "-h <type>    Harware type: Default: ${HW}"
    echo "             Supported values: any"
    echo "-o           Create squashfs/overlay based boot system"
    echo ""
    echo "If -b is specified then the boot files will be installed"
    echo "If -d is specified then the rootfs will be installed"
    echo "Default is to do nothing."
    echo "HW can be used to modify config.txt and/or cmdline.txt for specific build types."
    echo "e.g: HW=any means no changes."
    echo ""
}

# Print a message and exit with an error.
die()
{
    echo "Failure: $1"
    exit 1
}

# Make sure we have the tools required on the build machine.
validate_tools()
{
    TOOLS=()
    [[ "$(which uuidgen)" ]] || TOOLS+=( "Missing uuidgen" )
    [[ "$(which kpartx)"  ]] || TOOLS+=( "Missing kpartx" )

    if [[ ${#TOOLS[@]} -ne 0 ]]; then
        for entry in "${TOOLS[@]}"; do
            echo "${entry}"
        done
        die "Can't run $0 due to missing dependencies."
    fi
}

# Setup to use an image file as the device
doFile()
{
    echo "doFile: IMGFILE = ${IMGFILE} "

    if [[ ! -f "${IMGFILE}" ]]
    then
        echo "No such file: ${IMGFILE}"
        exit 1
    fi

    CNT=1
    # shellcheck disable=SC2024
    sudo kpartx -av "${IMGFILE}" > "$0".$$
    DEVICE="$(head -1 "$0".$$ | cut -f3 -d" " | cut -c1-5)"
    BOOTDIR=bootdir
    # shellcheck disable=SC2162
    while read line
    do
        partition="$(echo "${line}" | cut -f3 -d" ")"
        case ${CNT} in
        1)  # The boot device needs an offset to mount
            mkdir -p "${BOOTDIR}"
            BOOTDEVICE="$(sudo losetup --show -f "${IMGFILE}" -o 1048576)"
            sudo mount "${BOOTDEVICE}" "${BOOTDIR}"
            ;;
        2)  ROOTFSDIR=/dev/mapper/"${partition}";;
        esac
        CNT=$(( CNT + 1 ))
    done < "${0}".$$
    echo "doFile: BOOTDIR = ${BOOTDIR}"
    echo "doFile: ROOTFSDIR = ${ROOTFSDIR}"
    sudo rm -f "${0}".$$
}

# Cleanup from use of an image file as the device
doUnfile()
{
    sudo umount bootdir
    for id in 1 2 3; do
        if [[ -e "/dev/mapper/${DEVICE}p${id}" ]]; then
            sudo dmsetup remove "${DEVICE}p${id}"
        fi
    done
    sudo losetup -d "/dev/${DEVICE}"
    sudo losetup -d "${BOOTDEVICE}"
    rmdir bootdir
    losetup
}

# Copy the rootfs image to the root partition using dd.
unpack_rootfs()
{
    # Copy image to partition
    sudo e2label "${ROOTFSDIR}" rootfs
    sudo e2fsck -f "${ROOTFSDIR}"
    sudo resize2fs "${ROOTFSDIR}"

    # Set permissions on partitions files/directories
    sudo mkdir -p "${SDROOTFS}"
    sudo mount "${ROOTFSDIR}" "${SDROOTFS}"
    (
        # Do this in a subshell to avoid pushd/popd scenario.
        sudo unsquashfs -f -d "${SDROOTFS}/" "${ROOTFSARCHIVE}" 
        cd "${SDROOTFS}" >/dev/null || die "Can't change to ${SDROOTFS}"
        sudo chown -R root.root .
    )
    sudo umount "${SDROOTFS}"
    sudo rmdir "${SDROOTFS}"
}

# Generate overlay root partition
gen_overlay()
{
    echo "Generating overlay"
    local squashfs="${ROOTFSARCHIVE}"
    if [[ ! -e "${squashfs}" ]]; then
        echo "Can't find squashfs: ${squashfs}"
        return 1
    fi

    # Mount the root partition to a directory
    local rootfs
    rootfs="$(mktemp -d rootfs.XXXXX)"
    sudo mount "${ROOTFSDIR}" "${rootfs}"

    # Mount the user data partition to a directory
    local userfs
    userfs="$(mktemp -d userdata.XXXXX)"
    local userpart="${ROOTFSDIR/2/3}"
    sudo mount "${userpart}" "${userfs}"

    # Generate image id - this should be generated by the build system!
    local image_id
    image_id="$(uuidgen)"

    # Make the directory structure used by the overlay boot process.
    sudo mkdir -p "${rootfs}"/overlay/"${image_id}"/{presentation,upper,work,backups,squashfs,utils}
    sudo ln -sf "${image_id}" "${rootfs}"/overlay/current
    sudo mkdir -p "${userfs}"/{opt,home,log,opkg/pkg,opkg/db,opkg/incoming}

    # Copy in the squashfs
    echo "Installing squashfs to disk"
    local squashfs="${ROOTFSARCHIVE}"
    sudo rsync -a --info=progress2 "${squashfs}" "${rootfs}/overlay/${image_id}/squashfs"
    # ls -lR "${rootfs}"

    sudo umount "${userfs}"
    sudo rmdir "${userfs}"
    sudo umount "${rootfs}"
    sudo rmdir "${rootfs}"
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":of:F:b:r:u:l:s:t:d:h:" Option
do
    case ${Option} in
    b) BOOTDIR="${OPTARG}";;
    d) ROOTFSDIR="${OPTARG}";;
    l) UIMAGE="${OPTARG}";;
    s) BOOTSCR="${OPTARG}"
       DOBOOTSCR=1
       ;;
    t) UENV="${OPTARG}"
       DOUENV=1
       ;;
    r) ROOTFSARCHIVE="${OPTARG}";;
    f) DOFILE=1
       IMGFILE="${OPTARG}";;
    F) IMGFILE="${OPTARG}"
       doUnfile
       exit 0;;
    h) HW="${OPTARG}";;
    o) DOOVERLAY=1;;
    *) doHelp; exit 0;;
    esac
done

# Validate HW option.
case "${HW}" in
    any) ;;
    *)  echo "Invalid hardware specified (-h)."
        doHelp
        exit 0
        ;;
esac

# Make sure we have what we need installed on the build machine.
validate_tools

if [[ ${DOFILE} -eq 1 ]]
then
    doFile
fi
echo "BOOTDIR   = ${BOOTDIR}"
echo "ROOTFSDIR = ${ROOTFSDIR}"
if [[ ${DOOVERLAY} -eq 1 ]]
then
    echo "Building squashfs/overlay based boot system."
fi

#--------------------------------------------------------------
# Error checking
#--------------------------------------------------------------
if [[ "${BOOTDIR}" != "none" ]]
then
    if [[ ! -d "${BOOTDIR}" ]]; then
        echo "The boot directory (${BOOTDIR}) is not mounted.  Installation aborted."
        doHelp
        exit 1
    fi
    if [[ ! -f "${UIMAGE}" ]]; then
        echo "Your missing the Linux kernel binary.  Install aborted."
        doHelp
        exit 1
    fi
    if [[ ! -d "${FIRMWARE}" ]]; then
        echo "Your missing the ${FIRMWARE} directory.  Install aborted."
        doHelp
        exit 1
    fi
    if [[ ! -f "${UENV}" ]]; then
        echo "Your missing the uEnv.txt script.  Install aborted."
        doHelp
        exit 1
    fi
    if [[ ! -f "${BOOTSCR}" ]]; then
        echo "Your missing the boot.scr boot script.  Install aborted."
        doHelp
        exit 1
    fi
else
    echo "No boot directory specified. Boot files will not be installed."
fi

if [[ "${ROOTFSDIR}" != "none" ]]
then
    if [[ ! -b "${ROOTFSDIR}" ]]; then
        echo "The rootfs device (${ROOTFSDIR}) is not available.  Installation aborted."
        doHelp
        exit 1
    fi
    if [[ ${DOOVERLAY} -eq 0 ]] && [[ ! -f "${ROOTFSARCHIVE}" ]]; then
        echo "Your missing the rootfs image (${ROOTFSARCHIVE}).  Install aborted."
        doHelp
        exit 1
    fi
else
    echo "No root file system device specified. Rootfs archive will not be installed."
fi

if [[ "${BOOTDIR}" == "none" ]] && [[ "${ROOTFSDIR}" == "none" ]]
then
    doHelp
    exit 1
fi

#--------------------------------------------------------------
# Install firmware, kernel and configurations
#--------------------------------------------------------------
if [[ "${BOOTDIR}" != "none" ]]
then
    # First, copy in all the firmware
    echo "Installing firmware"
    sudo rsync -a --no-o --no-p --no-g --safe-links --modify-window 1 --info=progress2 \
        "${FIRMWARE}"/* "${BOOTDIR}"/

    # Now remove the upstream kernel images - we have our own.
    echo "Installing kernel"
    sudo rm "${BOOTDIR}"/kernel*

    # Copy in our kernel.
    sudo rsync -a --no-o --no-p --no-g --safe-links --modify-window 1 --info=progress2 \
        "${UIMAGE}" "${BOOTDIR}/${DESTUIMAGE}"
    echo "${DESTUIMAGE}" > kernelname.txt
    sudo cp kernelname.txt "${BOOTDIR}/"
    sudo rm kernelname.txt

    # Do we have special config.txt and cmdline.txt files to copy in?
    if [[ "${DOBOOTSCR}" -eq 1 ]]
    then
        echo "Installing cmdline.txt"
        sudo rsync -a --no-o --no-p --no-g --safe-links --modify-window 1 --info=progress2 \
            "${BOOTSCR}" "${BOOTDIR}"
    fi
    if [[ ${DOUENV} -eq 1 ]]
    then
        echo "Installing config.txt"
        sudo rsync -a --no-o --no-p --no-g --safe-links --modify-window 1 --info=progress2 \
            "${UENV}" "${BOOTDIR}"
    fi
    if [[ ${DOOVERLAY} -eq 1 ]]
    then
        echo "Installing initramfs"
        sudo rsync -a --no-o --no-p --no-g --safe-links --modify-window 1 --info=progress2 \
            "initramfs.gz" "${BOOTDIR}"
        if ! grep -q initramfs "${BOOTDIR}/$(basename "${UENV}")"; then
            cp "${UENV}" "/tmp/$(basename "${UENV}")"
            echo " initramfs initramfs.gz followkernel" >> "/tmp/$(basename "${UENV}")"
            sudo cp "/tmp/$(basename "${UENV}")" "${BOOTDIR}/$(basename "${UENV}")"
        fi
    fi

    # Modify config.txt/cmdline.txt for specific hardware.
    # This is less flexible than -s/-t, but it's easier to manage for PiBox specifics.
    case "${HW}" in
        any) ;;
    esac

fi

#--------------------------------------------------------------
# Install rootfs image
#--------------------------------------------------------------
if [[ "${ROOTFSDIR}" != "none" ]]
then
    if [[ ${DOOVERLAY} -eq 1 ]]; then
        gen_overlay
    else
        unpack_rootfs
    fi
fi

echo "Flushing writes to disk..."
sync;sync;sync

#--------------------------------------------------------------
# Cleanup image file setup
#--------------------------------------------------------------
if [[ ${DOFILE} -eq 1 ]]
then
    rm -f "${IMGFILE}.gz"
    echo "Compressing image..."
    gzip -9 "${IMGFILE}"
    tar -cJf "${IMGFILE}.tar.xz" "${IMGFILE}.gz" install.txt
    echo "Created ${IMGFILE}.tar.xz"
    echo "Cleaning up..."
    doUnfile
fi
