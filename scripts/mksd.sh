#!/bin/bash -p
# Format an SD card for use with Raspberry Pi
# Borrowed from Robert Nelson's instructions for the BeagleBoard
# http://elinux.org/BeagleBoardUbuntu#Partition_SD_Card
# --------------------------------------------

#--------------------------------------------------------------
# Initialization
#--------------------------------------------------------------
DODATA=1
DOFILE=0
DEVICE=
IMGFILE=pibox.img

#--------------------------------------------------------------
# Functions
#--------------------------------------------------------------
function msg
{
    echo ""
    echo "### $1"
}

#--------------------------------------------------------------
# Provide command line usage assistance
#--------------------------------------------------------------
function doHelp
{
    echo ""
    echo "$0 [-fn?| -d <device>]"
    echo "where"
    echo "-d <device>    Device to partition."
    echo "-n             Skip /data partition (for small SD cards <4GB)"
    echo "-f <file>      Generate image file ($IMGFILE) instead of actual device"
    echo ""
}

# Generate an image file instead of using an actual device
function doFile
{
    echo "Image File: ${IMGFILE}"

    # Cleanup any previous image file
    if [[ -f "${IMGFILE}" ]]
    then
        rm -f "${IMGFILE}"
    fi

    # Allocate an image file
    msg "Creating blank image file"
    dd if=/dev/zero of="${IMGFILE}" bs=1M seek=2500 count=0

    # Add a legacy partition table (not GPT)
    msg "Creating legacy partition table"
    sudo parted "${IMGFILE}" mktable msdos

    # Create boot partition
    msg "Creating partition 1: boot"
    sudo parted -s "${IMGFILE}" -- mkpart p fat16 1 128

    # Create root file system partition
    msg "Creating partition 2: root file system"
    sudo parted -s "${IMGFILE}" -- mkpart p ext2 129 2128

    # Create a stub of a data partition.
    # Run time tools can expand this partition later.
    msg "Creating partition 3 stub: data"
    sudo parted -s "${IMGFILE}" -- mkpart p ext2 2129 -1

    # Mount the image file so we can access the partitions
    CNT=1
    # shellcheck disable=SC2024
    sudo kpartx -av "${IMGFILE}" > "$0.$$"
    device="$(head -1 "$0.$$" | cut -f3 -d" " | cut -c1-5)"
    # shellcheck disable=SC2162
    while read line
    do
        partition="$(echo "${line}" | cut -f3 -d" ")"
        msg "Partition ${CNT}: ${partition}"

        # Create file systems on the partitions.
        case ${CNT} in
        1) sudo mkfs.vfat -F 16 /dev/mapper/"${partition}" -n BOOT;;
        2) sudo mkfs.ext3 -F -F -L rootfs /dev/mapper/"${partition}";;
        3) sudo mkfs.ext3 -F -F -L data /dev/mapper/"${partition}";;
        esac
        sudo dmsetup remove "${partition}"
        CNT=$(( CNT + 1 ))
    done < "$0.$$"

    # Unmount the image file
    sudo losetup -d "/dev/${device}"
    rm -f "$0.$$"
}

#--------------------------------------------------------------
# Read command line arguments
#--------------------------------------------------------------
while getopts ":nd:f:" Option
do
    case $Option in
    n) DODATA=0;;
    d) DEVICE="${OPTARG}";;
    f) IMGFILE="${OPTARG}"
       DOFILE=1
       ;;
    *) doHelp; exit 0;;
    esac
done

if [[ ${DOFILE} -eq 1 ]]
then
    doFile
    exit 0
fi

if [[ "${DEVICE}" = "" ]]
then
    echo "You must supply the SD device on the command line (-d option)."
    doHelp
    exit 1
fi

if [[ ! -b "${DEVICE}" ]]
then
    echo "${DEVICE} does not exist or is not a block device."
    exit 1
fi

#--------------------------------------------------------------
# Main
#--------------------------------------------------------------
# Remove any previous partitions
msg "Cleaning old partitions (if they exist)"
sudo parted "${DEVICE}" -- rm 1
sudo parted "${DEVICE}" -- rm 2
sudo parted "${DEVICE}" -- rm 3

# blank the MMC card
msg "Creating msdos label"
if ! sudo parted -s "${DEVICE}" mklabel msdos
then
    echo "Failed to make label on first partition."
    exit 1
fi

# Create partitions
msg "Creating partition 1"
sudo parted -s "${DEVICE}" mkpart p fat16 1 128
msg "Creating partition 2"
if [[ ${DODATA} -eq 1 ]]
then
    sudo parted -s "${DEVICE}" mkpart p ext2 129 2128
    msg "Creating partition 3"
    sudo parted "${DEVICE}" -- mkpart p ext2 2128 -1
else
    sudo parted -s "${DEVICE}" -- mkpart p ext2 65 -1
fi

# set the partition boot flag
sudo parted "${DEVICE}" set 1 boot on

MMC="$(echo "${DEVICE}" | grep mmc)"
echo "MMC=${MMC}"

# Format the first partition
if [[ "${MMC}" = "" ]]
then
    sudo mkfs.vfat -F 16 "${DEVICE}"1 -n BOOT
else
    sudo mkfs.vfat -F 16 "${DEVICE}"p1 -n BOOT
fi

# Format rootfs partition
if [[ "$MMC" = "" ]]
then
    sudo mkfs.ext3 -F -F -L rootfs "${DEVICE}"2
else
    sudo mkfs.ext3 -F -F -L rootfs "${DEVICE}"p2
fi

# Format data partition
if [[ ${DODATA} -eq 1 ]]
then
    if [[ "${MMC}" = "" ]]
    then
        sudo mkfs.ext3 -F -F -L data "${DEVICE}"3
    else
        sudo mkfs.ext3 -F -F -L data "${DEVICE}"p3
    fi
fi
